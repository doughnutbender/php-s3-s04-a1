<?php 
/*Instuctions:
1. Create a Product class with five properties: name, price, short description, category, stock no.
2. Create two child classes for Product class: Mobile and Computer
3. Create a method called printDetails in each of the classes and must have following input:
	Product: "The product has a name of  <productname> and its price is <productprice>, and the stock no. is <product stock>"
	Mobile: "Our latest mobile is <mobile name> with a cheapest price of <mobile price>"
	Computer: "Our newest computer is <computer name> and its base price is <computer price>"
4. On the Product class, create the following getters and setters:
	price
	stock no.,
	category

5. Kindly create an instance of the following

	Product:
		name - Xioami Mi Monitor
		price - 22,000.00
		short description - Good for gaming and for coding
		stock no - 5
		category - computer-peripherals

	Mobile
		name -Xioami Redmi Note 10 Pro
		price - 13590.00
		short description - Latest Xioami phone ever made
		stock no - 10
		category - mobiles and elecctronics

	Computer
		name - HP Business Laptop
		price - 29000
		short description - HP Laptop only made for business
		stock no - 10
		category - laptops and computers

6. Display the details of an instance of a class of the ff:
	Product - display it on the first list item of the ul tag in the index.php
	Mobile - display it on the second list item of the ul tag in the index.php
	Computer - display it on the last list item of the ul tag in the index.php

7. Kindly update the following details of the objects:

	Product object - update its stock no from 5 to 3
	Mobile object - update the stock no. from 10 to 5
	Computer object - update the catergory from 'laptops and computers' to 'laptops, computers and electronics'

*/


class Product {
	public $name, $price, $shortDescription, $category, $stockNo;

	public function __construct($nameValue, $priceValue, $shortDescriptionValue, $stockNoValue, $categoryValue){
			$this ->name = $nameValue;
			$this ->price =$priceValue;
			$this ->shortDescription = $shortDescriptionValue;
			$this ->stockNo = $stockNoValue;
			$this ->category = $categoryValue;

		}
	public function printDetails(){
		return "The product has a name of  $this->name and its price is $this->price, and the stock no. is $this->stockNo.";
		}

	public function getPrice(){
		return $this->price;
		}
	public function getStockNo(){
		return $this->stockNo;
		}
	public function getCategory(){
		return $this->category;
		}

	public function setPrice($priceValue){
		$this->price = $priceValue;
		}
	public function setStockNo($stockNoValue){
		$this->stockNo = $stockNoValue;
		}
	public function setCategory($categoryValue){
		$this->category = $categoryValue;
		}

}


class Mobile extends Product{
	public function printDetails(){
		return "Our latest mobile is $this->name with a cheapest price of $this->price.";
		}
}

class Computer extends Product{
	public function printDetails(){
	return "Our newest computer is $this->name and its base price is $this->price.";
	}
}

$newProduct = new Product('Xioami Mi Monitor', 22000.00, 'Good for gaming and for coding',5,'computer-peripherals');

$newMobile = new Mobile('Xioami Redmi Note 10 Pro', 13590.00, 'Latest Xioami phone ever made', 10, 'mobiles and elecctronics');

$newComputer = new Computer('HP Business Laptop', 29000.00, 'HP Laptop only made for business', 10, 'laptops and computers');

 ?>
