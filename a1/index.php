<?php require "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>PHP OOP | Activity</title>
</head>
<body>

	<ul>
		<li><?php echo $newProduct->printDetails(); ?></li>
			
		<li><?php echo $newMobile->printDetails(); ?></li>
		<li><?php echo $newComputer->printDetails(); ?></li>
	</ul>

<?php $newProduct->setStockNo(3); ?>
<?php $newMobile->setStockNo(5); ?>
<?php $newComputer->setCategory('laptops, computers and electronics'); ?>

	<ul>
		<li><?php echo $newProduct->printDetails(); ?></li>
		<li><?php echo $newMobile->printDetails(); ?>
		Stock number is 
		<?php echo $newMobile->getStockNo();?>.</li>
		<li><?php echo $newComputer->printDetails(); ?>
		Category is 
		<?php echo $newComputer->getCategory();?>.
		</li>
	</ul>

</body>
</html>