<?php require "./code.php"; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>PHP Classes and Objects</title>
</head>
<body>
	<h1>PHP Classes and Objects</h1>
	
	<?php $newCondoUnit->name; ?>
	<pre>
		<?php 
			print_r($buildingObj);
		 ?>
	</pre>

	<pre>
		<?php 
			print_r($newBuilding);
		 ?>
	</pre>

	<pre>
		<?php 
			print_r($newPerson);
		 ?>
	</pre>
	<p>
		<?php echo $newBuilding->getBuildingDetails(); ?>

	</p>
	<?php $newBuilding->setFloors(6) ?>
	<ul>
		<li>
			Building name: <?php echo $newBuilding->getName(); ?>
		</li>
		<li>
			Floor no.: <?php echo $newBuilding->getFloors(); ?>
		</li>
		<li>
			Address: <?php echo $newBuilding->getAddress(); ?>
		</li>
	</ul>

	<p>
		<?php echo $newPerson->getPersonDetails(); ?>
	</p>
	<?php $newPerson->setMName("Tamad"); ?>
	<p>
		<?php echo $newPerson->getPersonDetails(); ?>
	</p>

	<ul>
		<li>
			First name: <?php echo $newPerson->getfName(); ?>
		</li>
		<li>
			Middle name: <?php echo $newPerson->getmName(); ?>
		</li>
		<li>
			Last name: <?php echo $newPerson->getlName(); ?>
		</li>
	</ul>
	<hr>
	<p>
		<?php echo $newCondoUnit->getBuildingDetails(); ?>
	</p>
</body>
</html>