<?php 
	
	$buildingObj = (object)[
		'name' => 'Casswynn Bldg',
		'floors' => 8,
		'address' => (object)[
			'barangay' => 'Sacred Heart',
			'city' => 'Quezon City',
			'country' => 'Philippines'
		]
	];

	class Building { //creates a blueprint/base class of our Building
		//public keyword - visibility level of how the methods/properties ca be access by the outsiders
		//public, private, protected
		//protected - allows the children/subclass to inherit methods/properties of the class, however if the outsiders wants to have a direct access to the methods/properties it will disable it
		//private - disables the direct access and disablesthe inheritance to its child classes;

		private $name; //publicly, the $name, $floor and $address are visible to the outsiders
		public $floors;
		public $address;

		//Constructor Functions - special type of functions which is called automatically whenever we are instantiating an object (creating an instances of a class)
		public function __construct($nameValue, $floorsValue, $addressValue){
			//$this - keyword is a reserved keyword in PHP that refers to class itself
			$this ->name = $nameValue;
			$this ->floors = $floorsValue;
			$this ->address = $addressValue;
		}
		//function to print the data

		public function getBuildingDetails(){ 
		//once a function is created inside a class, it is called a method
			return "$this->name, $this->floors and $this->address";
		}

		public function getName(){ //getter
			return $this->name;
		}

		public function getFloors(){ 
			return $this->floors;
		}

		public function getAddress(){ 
			return $this->address;
		}

		public function setFloors($floorsValue){ //setter / update
			$this->floors = $floorsValue;
		}
	}

	//instantiate an object using the class Building
	//$newBuilding will contain yung new object
	$newBuilding = new Building('Enzo Bldg', 5, 'Buendia Avenue, Makati City, Philippines');

	class Condominium extends Building{
		//Condominium class inherits the properties/methods of the base class Building
		//It means that condominiums also have name, floors, and address

		//polymorphism
		public function getBuildingDetails(){
			return "These are the condominium details: name $this->name, floors $this->floors, address $this->address";
		}
	}

	$newCondoUnit = new Condominium('Avida Land', 120, 'Taguig, Philippines');

	class Person {
		public $firstName, $middleName, $lastName;
		public $age;
		public $birthday;

		public function __construct($fNameValue, $mNameValue, $lNameValue, $ageValue, $birthdayValue){
			$this ->firstName = $fNameValue;
			$this ->middleName =$mNameValue;
			$this ->lastName = $lNameValue;
			$this ->age = $ageValue;
			$this ->birthday = $birthdayValue;
		}
		public function getPersonDetails(){ 
			return "$this->firstName $this->middleName $this->lastName";
		}

		public function getfName(){ //getter
			return $this->firstName;
		}

		public function getmName(){ 
			return $this->middleName;
		}

		public function getlName(){ 
			return $this->lastName;
		}

		public function setFName($fNameValue){
			$this->firstName = $fNameValue;
		}
		public function setMName($mNameValue){
			$this->middleName = $mNameValue;
		}
		public function setLName($LNameValue){
			$this->lastName = $lNameValue;
		}		
	}

	$newPerson = new Person('Pedro', 'Masipag', 'Penduko', 28, 'January 1, 1993');
?>